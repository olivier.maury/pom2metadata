# SPDX-FileCopyrightText: 2023-present Olivier Maury <Olivier.Maury@inrae.fr>
#
# SPDX-License-Identifier: GPL-3.0-only

import logging
import os.path
import yaml
from . import pomxml

def _create(path: str, pom: pomxml.Pom):
	logging.info('Creating %s' % path)
	citation = {'cff-version': '1.2.0'}
	citation['message'] = 'You can cite %s by using the following metadata' % pom.name
	citation['title'] = pom.name
	citation['abstract'] = pom.description
	citation['authors'] = []
	citation['keywords'] = ['Java']
	citation['version'] = pom.version
	citation['doi'] = ''
	citation['date-released'] = pom.release_date
	citation['license'] = pom.license_name
	citation['repository-code'] = pom.scm_url
	with open(path, 'w') as file:
		yaml.dump(citation, file, default_flow_style=False, sort_keys=False)

def _update(path: str, pom: pomxml.Pom):
	logging.info('Updating %s' % path)
	with open(path, 'r') as file:
		citation = yaml.safe_load(file)

	citation['date-released'] = pom.release_date
	citation['repository-code'] = pom.scm_url
	citation['version'] = pom.version

	with open(path, 'w') as file:
		yaml.dump(citation, file, default_flow_style=False, sort_keys=False)

def handle(pom: pomxml.Pom):
	path = 'CITATION.cff'
	if os.path.exists(path):
		_update(path, pom)
	else:
		_create(path, pom)
