# SPDX-FileCopyrightText: 2023-present Olivier Maury <Olivier.Maury@inrae.fr>
#
# SPDX-License-Identifier: GPL-3.0-only
__version__ = "0.2.0"
