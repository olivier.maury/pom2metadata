# SPDX-FileCopyrightText: 2023-present Olivier Maury <Olivier.Maury@inrae.fr>
#
# SPDX-License-Identifier: GPL-3.0-only

from datetime import date
import logging
import os.path
import xml.etree.ElementTree as ET

class Developer:
	name = None
	github = None
	gitlab = None
	gitlab_id = None
	orcid = None

class Pom:
	description = {'en': ''}
	developers = []
	java_version = ''
	license_name = ''
	long_description = {}
	name = ''
	organization_name = ''
	release_date = date.today()
	scm_url = ''
	url = ''
	version = ''

def find_text(element: ET.Element, match: str) -> str:
	if element is None:
		return None
	found = element.find(match)
	if found is not None:
		return found.text
	logging.warning('Element "%s" not found in %s' % (match, element))
	return None

def find_text_with_lang(element: ET.Element, match: str) -> dict:
	found = element.findall(match)
	d = {}
	if found is not None:
		for e in found:
			lang = e.get('{http://www.w3.org/XML/1998/namespace}lang')
			if lang is None:
				lang = 'en'
			d[lang] = e.text
		return d
	logging.warning('Element "%s" not found in %s' % (match, element))
	return d

def parse(path: str) -> Pom:
	if not os.path.exists('pom.xml'):
		logging.error('pom.xml does not exist in the current directory.')
		sys.exit(1)
	logging.info('Opening %s' % path)
	tree = ET.parse(path)
	root = tree.getroot()
	pom = Pom()
	pom.description['en'] = find_text(root, '{http://maven.apache.org/POM/4.0.0}description')
	pom.name = find_text(root, '{http://maven.apache.org/POM/4.0.0}name')
	pom_organization = root.find('{http://maven.apache.org/POM/4.0.0}organization')
	pom.organization_name = find_text(pom_organization, '{http://maven.apache.org/POM/4.0.0}name')
	pom.version = find_text(root, '{http://maven.apache.org/POM/4.0.0}version')
	pom_scm = root.find('{http://maven.apache.org/POM/4.0.0}scm')
	pom.scm_url = find_text(pom_scm, '{http://maven.apache.org/POM/4.0.0}url')
	pom_properties = root.find('{http://maven.apache.org/POM/4.0.0}properties')
	descriptions = find_text_with_lang(pom_properties, '{http://maven.apache.org/POM/4.0.0}metadata.description')
	for lang in descriptions:
		pom.description[lang] = descriptions[lang]
	pom.java_version = find_text(pom_properties, '{http://maven.apache.org/POM/4.0.0}maven.compiler.source')
	pom.long_description = find_text_with_lang(pom_properties, '{http://maven.apache.org/POM/4.0.0}metadata.long-description')
	pom_licenses = root.find('{http://maven.apache.org/POM/4.0.0}licenses')
	pom.license_name = ''
	if pom_licenses is not None and len(pom_licenses) > 0:
		pom.license_name = find_text(pom_licenses[0], '{http://maven.apache.org/POM/4.0.0}name')
	url = root.find('{http://maven.apache.org/POM/4.0.0}url')
	if url is not None:
		pom.url = url.text
	for developers in root.iter('{http://maven.apache.org/POM/4.0.0}developers'):
		for x in developers.iter('{http://maven.apache.org/POM/4.0.0}developer'):
			developer = Developer()
			developer.name = x.find('{http://maven.apache.org/POM/4.0.0}name').text;
			props = x.find('{http://maven.apache.org/POM/4.0.0}properties')
			if props is not None:
				developer.github = find_text(props, '{http://maven.apache.org/POM/4.0.0}metadata.github')
				developer.gitlab_id = find_text(props, '{http://maven.apache.org/POM/4.0.0}metadata.gitlab-id')
				developer.gitlab = find_text(props, '{http://maven.apache.org/POM/4.0.0}metadata.gitlab')
				developer.orcid = find_text(props, '{http://maven.apache.org/POM/4.0.0}metadata.orcid')
			pom.developers.append(developer)
	return pom

