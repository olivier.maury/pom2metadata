# SPDX-FileCopyrightText: 2023-present Olivier Maury <Olivier.Maury@inrae.fr>
#
# SPDX-License-Identifier: GPL-3.0-only

import logging
import os.path
import yaml
from . import pomxml

COMMENTS = """# This repository adheres to the publiccode.yml standard by including this 
# metadata file that makes public software easily discoverable.
# More info at https://github.com/italia/publiccode.yml
"""

def _create(path: str, pom: pomxml.Pom):
	logging.info('Creating %s' % path)
	publiccode = {
		'publiccodeYmlVersion': '0.2',
		'categories': [],
		'dependsOn': {
			'open': [
				{
					'name': 'java',
					'optional': False,
					'versionMin': pom.java_version
				},
			],
		},
		'description': {},
		'developmentStatus': 'development',
		'landingURL': pom.url,
		'legal': {
			'authorsFile': 'AUTHORS.md',
			'license': pom.license_name,
		},
		'name': pom.name,
		'releaseDate': pom.release_date,
		'softwareType': 'standalone/desktop',
		'softwareVersion': pom.version,
		'url': pom.scm_url,
		'usedBy': [pom.organization_name]
	}
	for lang in pom.description:
		publiccode['description'][lang] = {
			'features': [],
			'genericName': pom.name,
			'shortDescription': pom.description[lang],
		}
		if lang in pom.long_description:
			publiccode['description'][lang]['longDescription'] = pom.long_description[lang]
	with open(path, 'w') as file:
		file.write(COMMENTS)
		yaml.dump(publiccode, file, default_flow_style=False, sort_keys=False)

def _update(path: str, pom: pomxml.Pom):
	logging.info('Updating %s' % path)
	with open(path, 'r') as file:
		publiccode = yaml.safe_load(file)

	publiccode['landingURL'] = pom.url
	publiccode['releaseDate'] = pom.release_date
	publiccode['softwareVersion'] = pom.version
	publiccode['url'] = pom.scm_url
	for lang in pom.description:
		publiccode['description'][lang]['shortDescription'] = pom.description[lang]
		if lang in pom.long_description:
			publiccode['description'][lang]['longDescription'] = pom.long_description[lang]

	with open(path, 'w') as file:
		yaml.dump(publiccode, file, default_flow_style=False, sort_keys=False)

def handle(pom: pomxml.Pom):
	path = 'publiccode.yml'
	if os.path.exists(path):
		_update(path, pom)
	else:
		_create(path, pom)
