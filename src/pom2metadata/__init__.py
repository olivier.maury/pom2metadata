# -*- coding: UTF-8 -*-

import logging
import os.path
import sys
import yaml

from . import citation
from . import pomxml
from . import publiccode

def create_authors(pom: pomxml.Pom):
	logging.info('Creating AUTHORS.md')
	with open('AUTHORS.md', 'w') as file:
		file.write('# Authors\n')
		for x in pom.developers:
			file.write('\n## %s\n' % x.name)
			if x.github is not None:
				file.write('- [GitHub](%s)\n' % x.github)
			if x.gitlab_id is not None and x.gitlab is not None:
				file.write('- GitLab : [%s](%s)\n' % (x.gitlab_id, x.gitlab))
			if x.orcid is not None:
				file.write('- [ORCID](%s)\n' % x.orcid)

def create_sbom():
	logging.info('Creating SBOM (codemeta.json)')
	try:
		from codemeta import codemeta
	except ModuleNotFoundError:
		logging.error("Codemetapy not found. Try pip install codemetapy")
		sys.exit(1)
	sys.argv = sys.argv + ['pom.xml', '-O', 'codemeta.json']
	codemeta.main()

def main():
	logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
	pom = pomxml.parse('pom.xml')
	create_authors(pom)
	create_sbom()
	citation.handle(pom)
	publiccode.handle(pom)
	logging.info('Done')

