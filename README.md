# pom2metadata

`pom2metadata` is a command line tool to create or update metadata project files from information stored in the Maven `pom.xml` file.

The metadata handled are:
- `AUTHORS.md`,
- [`CITATION.cff`](https://citation-file-format.github.io/),
- [`codemeta.json`](https://codemeta.github.io/),
- [`publiccode.yml`](https://docs.italia.it/italia/developers-italia/publiccodeyml-en/).

It uses [CodeMetaPy](https://pypi.org/project/CodeMetaPy/) to create or update `codemeta.json` ([codemeta](https://codemeta.github.io/) software metadata standard in JSON-LD).

[![Hatch project](https://img.shields.io/badge/%F0%9F%A5%9A-Hatch-4051b5.svg)](https://github.com/pypa/hatch)

## Installation

```
pip install pom2metadata
```

## Usage

In the source directory where `pom.xml` is:

```
pom2metadata
```

The Maven file `pom.xml` will be parsed to create or update the various metadata files.

Some new tags where added to fill fields in metadata files as much as possible:

- in `developers > developer > properties`:
    - `metadata.gitlab-id`: identifier in GitLab, such as `@firstname.familyname`,
    - `metadata.gitlab`: user's home page at GitLab, such as `https://gitlab/firstname.familyname`,
    - `metadata.orcid`: ORCID URL, such as `https://orcid.org/0000-0001-2345-6789`.
- in `properties`:
    - `metadata.description`, with attribute `xml:lang` to set translations for `description`,
    - `metadata.long-description`, with attribute `xml:lang` to define the long description in all languages.

Moreover, take care of some tags:

- as [recommended](https://maven.apache.org/pom.html#licenses), `licenses > license > name` should use a [SPDX identifier](https://spdx.org/licenses/).

## Development

### Preparing

- Install [hatch](https://hatch.pypa.io/):
```
sudo apt install pipx
pipx install hatch
```
- Make sure `$HOME/.local/bin` is in the `PATH`
```
pipx ensurepath
```

### Developing

- Create environment: `pipx run hatch env create`.
- Run: `pipx run hatch env run pom2metadata`.

To update version: `hatch version 0.1.0'.

### Install local project

To install from sources, with pip, there are two ways to do this for:

* a regular install: from the source directory, `pip install`, or from elsewhere `pip install path/to/pom2metadata`,
* an [editable install](https://pip.pypa.io/en/stable/topics/local-project-installs/#editable-installs): `pip install -e path/to/pom2metadata`.

If your installed pip is too old (and handles only `setup.py` files), use the Hatch environment:
```
hatch shell
pip install -e path/to/pom2metadata
```

### Generating distribution archives

To generate [distribution packages](https://packaging.python.org/en/latest/glossary/#term-Distribution-Package) - archives that are uploaded to the Python Package Index and can be installed by pip - for the package using with [hatch](https://hatch.pypa.io/latest/build/): `hatch build`.

## Authors

See [`AUTHORS.md`](AUTHORS.md) file.

## License

See [`License.md`](License.md) file.
